<?php

namespace Game\Controllers\Impl; 

use Beweb\Td\Dal\DAOCharacter;
use Beweb\Td\Engines\Dome;

class Game {
    /**
     * Start the game to know who's gonna win
     *
     * @return void
     */
    public function launch() {
        $dao = new DAOCharacter();
        Dome::getInstance()->addAll($dao->load());
        Dome::getInstance()->start();
    }

}