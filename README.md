Configuration :

- docker run -i -t -p "80:80" -v ${PWD}/app:/app -v ${PWD}/mysql:/var/lib/mysql mattrayner/lamp:latest
- copier dans /etc/apache2/sites-available/000-default.conf

<Directory /var/www/html>
    Options Indexes FollowSymLinks MultiViews
    # To make wordpress .htaccess work
    AllowOverride All
    Order allow,deny
    allow from all
</Directory>

- créer un fichier .htaccess et coller 

RewriteEngine On
RewriteRule ^([a-zA-Z0-9\-\_\/]*)$ index.php?p=$1

- taper a2enmod rewrite

- REDEMARRER !!!!!!!

- Entrer dans la barre d'un navigateur \\wsl$\kali-linux\home pour voir l'arborescence de fichiers

