<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit933abdf0b38f7902eed7150716166ddd
{
    public static $prefixLengthsPsr4 = array (
        'G' => 
        array (
            'Game\\Controllers\\Impl\\' => 22,
            'Game\\Controllers\\Core\\' => 22,
        ),
        'B' => 
        array (
            'Beweb\\Td\\Models\\Interfaces\\' => 27,
            'Beweb\\Td\\Models\\Impl\\Race\\' => 26,
            'Beweb\\Td\\Models\\Impl\\Job\\' => 25,
            'Beweb\\Td\\Models\\' => 16,
            'Beweb\\Td\\Engines\\' => 17,
            'Beweb\\Td\\Dal\\' => 13,
            'Beweb\\Td\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Game\\Controllers\\Impl\\' => 
        array (
            0 => __DIR__ . '/../..' . '/controllers',
        ),
        'Game\\Controllers\\Core\\' => 
        array (
            0 => __DIR__ . '/../..' . '/core',
        ),
        'Beweb\\Td\\Models\\Interfaces\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src/Models/Interfaces',
        ),
        'Beweb\\Td\\Models\\Impl\\Race\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src/Models/Impl',
        ),
        'Beweb\\Td\\Models\\Impl\\Job\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src/Models/Impl',
        ),
        'Beweb\\Td\\Models\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src/Models',
        ),
        'Beweb\\Td\\Engines\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src/Engines',
        ),
        'Beweb\\Td\\Dal\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src/Dal',
        ),
        'Beweb\\Td\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dome/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit933abdf0b38f7902eed7150716166ddd::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit933abdf0b38f7902eed7150716166ddd::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit933abdf0b38f7902eed7150716166ddd::$classMap;

        }, null, ClassLoader::class);
    }
}
